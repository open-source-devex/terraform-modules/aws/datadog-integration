variable "name_prefix" {
  description = "Name prefix for all resources that will take them"
  type        = string
  default     = ""
}

variable "tags" {
  description = "Tags appended to all resources that will take them"
  type        = map(string)
  default     = {}
}

variable "enable_security_audit_iam" {
  description = "Whether to attach the SecurityAudit IAM policy to the DataDog integration role."
  type        = bool
  default     = false
}

variable "enable_datadog_forwarder_stack" {
  description = "Whether to enable the DD forwarder lambda stack"
  type        = bool
  default     = false
}

variable "enable_datadog_rds_stack" {
  description = "Whether to enable the DD RDS Enhanced lambda stack"
  type        = bool
  default     = false
}

variable "datadog_api_key" {
  description = "The API key used for the DataDog provider"
  type        = string
  default     = ""
}

variable "create_datadog_api_key_secret" {
  description = "Whether to create a AWS SecretsManager secret for DD API key, when set to false `datadog_api_key_secret_arn` must be set to a valid ARN"
  type        = bool
  default     = true
}

variable "datadog_api_key_secret_arn" {
  description = "The ARN of a pre-existing AWS SecretsManager secret containing the DD API key to be used in the forwarding stack. Conflicts with `create_forwarder_api_key_secret`."
  type        = string
  default     = ""
}

variable "datadog_site" {
  description = "The DD site to set as a parameter to the cloudFormation stack"
  type        = string
  default     = "datadoghq.eu"
}

variable "aws_account_id" {
  description = "The ID of the AWS account to integrate with DD"
  type        = string
  default     = null
}

variable "filter_tags" {
  description = "Tags used to filter which resources should be monitored by datadog. Set to empty map to monitor all resources."
  type        = map(string)

  default = {
    "com.datadoghq.app" = "enabled"
  }
}

variable "datadog_code_s3_bucket" {
  description = "The bucket where the lambda function packages can be found."
  type        = string
  default     = null
}

variable "account_specific_namespace_rules" {
  type        = map(bool)
  description = "Enable/disable specific AWS services from being monitored. Defaults to all enabled"
  default = {
    "api_gateway"            = true
    "application_elb"        = true
    "apprunner"              = true
    "appstream"              = true
    "appsync"                = true
    "athena"                 = true
    "auto_scaling"           = true
    "billing"                = true
    "budgeting"              = true
    "certificatemanager"     = true
    "cloudfront"             = true
    "cloudhsm"               = true
    "cloudsearch"            = true
    "cloudwatch_events"      = true
    "cloudwatch_logs"        = true
    "codebuild"              = true
    "cognito"                = true
    "collect_custom_metrics" = true
    "connect"                = true
    "crawl_alarms"           = true
    "directconnect"          = true
    "dms"                    = true
    "documentdb"             = true
    "dynamodb"               = true
    "ebs"                    = true
    "ec2"                    = true
    "ec2api"                 = true
    "ec2spot"                = true
    "ecs"                    = true
    "efs"                    = true
    "elasticache"            = true
    "elasticbeanstalk"       = true
    "elasticinference"       = true
    "elastictranscoder"      = true
    "elb"                    = true
    "emr"                    = true
    "es"                     = true
    "firehose"               = true
    "fsx"                    = true
    "gamelift"               = true
    "glue"                   = true
    "inspector"              = true
    "iot"                    = true
    "kinesis"                = true
    "kinesis_analytics"      = true
    "kms"                    = true
    "lambda"                 = true
    "lex"                    = true
    "mediaconnect"           = true
    "mediaconvert"           = true
    "mediapackage"           = true
    "mediatailor"            = true
    "ml"                     = true
    "mq"                     = true
    "msk"                    = true
    "nat_gateway"            = true
    "neptune"                = true
    "network_elb"            = true
    "networkfirewall"        = true
    "opsworks"               = true
    "polly"                  = true
    "rds"                    = true
    "redshift"               = true
    "rekognition"            = true
    "route53"                = true
    "route53resolver"        = true
    "s3"                     = true
    "s3storagelens"          = true
    "sagemaker"              = true
    "ses"                    = true
    "shield"                 = true
    "sns"                    = true
    "sqs"                    = true
    "step_functions"         = true
    "storage_gateway"        = true
    "swf"                    = true
    "transitgateway"         = true
    "translate"              = true
    "trusted_advisor"        = true
    "usage"                  = true
    "vpn"                    = true
    "waf"                    = true
    "wafv2"                  = true
    "workspaces"             = true
    "xray"                   = true
  }
}

##################################################################
# Log forwarding stack
##################################################################
variable "datadog_forwarder_use_vpc" {
  description = "Whether to use VPC or not."
  type        = bool
  default     = false
}

variable "datadog_forwarder_vpc_security_group_ids" {
  description = "The VPC security group ids to use. Comma separated list."
  type        = string
  default     = ""
}

variable "datadog_forwarder_vpc_subnet_ids" {
  description = "The VPC subnets to use. Comma separated list."
  type        = string
  default     = ""
}

variable "datadog_forwarder_bucket_subscriptions" {
  description = "Map of bucket names to a pairs of bucket ARN and KMS key ARN. This map is used to configure forwarding logs from the respective buckets to DD."

  type = map(object({
    bucket_arn   = string
    is_encrypted = bool
    kms_key_arn  = string
  }))

  default = {
    # bucket-name = {
    #   bucket_arn  = "arn"
    #   kms_key_arn = "arn"
    # }
  }
}

variable "datadog_forwarder_cloudwatch_event_subscriptions" {
  description = "Map of CloudWatch event subscriptions."

  type = map(object({
    source      = string
    detail_type = string
  }))

  default = {
    # guardduty-finding = {
    #   source = "aws.guardduty"
    #   detail_type = "GuardDuty Finding"
    #
  }
}

variable "datadog_forwarder_cloudwatch_log_group_subscriptions" {
  description = "Map of CloudWatch log group subscriptions."

  type = map(object({
    log_group_name              = string
    subscription_filter_pattern = string
  }))

  default = {
    #    subscription_name = {
    #      log_group_name = "the-log-group"
    #      subscription_filter_pattern = "" # empty to send all the logs
    #    }
  }
}

variable "datadog_forwarder_include_at_match" {
  description = "Regex pattern on which to filter. if set to empty string, no filtering is applied"
  type        = string
  default     = ""
}

variable "custom_rds_stack_python_version" {
  description = "Python version to use for Lambda Function"
  type        = string
  default     = "python3.7"
}

##################################################################
# RDS forwarding stack
##################################################################
variable "datadog_rds_use_vpc" {
  description = "Whether to use VPC or not."
  type        = bool
  default     = false
}

variable "datadog_rds_vpc_security_group_ids" {
  description = "The VPC security group ids to use. Comma separated list."
  type        = string
  default     = ""
}

variable "datadog_rds_vpc_subnet_ids" {
  description = "The VPC subnets to use. Comma separated list."
  type        = string
  default     = ""
}

variable "datadog_rds_configure_subscription" {
  description = "Whether to enable the CloudWatch log subscription and ship logs to DD"
  type        = bool
  default     = false
}

variable "custom_datadog_rds_stack_template" {
  description = "A stack template to be used instead of the default DD stack template"
  type        = string
  default     = null
}

variable "custom_rds_stack_kms_key_policy" {
  description = "A addition KMS Key policy"
  type = list(object({
    principals = list(object({
      type = string, identifiers = list(string)
    }))
    effect    = string
    actions   = list(string)
    resources = list(string)
    condition = list(object({
      test     = string
      variable = string
      values   = list(string)
    }))
  }))
  default = []
}

variable "datadog_rds_cloudwatch_log_group_name" {
  description = "The name of the CloudWatch log group for RDS OS metrics"
  type        = string
  default     = "RDSOSMetrics"
}

variable "datadog_rds_cloudwatch_subscription_filter_pattern" {
  description = "A valid CloudWatch Logs filter pattern for subscribing to a filtered stream of log events."
  type        = string
  default     = ""
}

variable "datadog_rds_cloudwatch_subscription_distribution" {
  description = "The method used to distribute log data to the destination. By default log data is grouped by log stream, but the grouping can be set to random for a more even distribution. This property is only applicable when the destination is an Amazon Kinesis stream. Valid values are \"Random\" and \"ByLogStream\"."
  type        = string
  default     = "Random"
}

variable "datadog_rds_code_s3_object" {
  description = "The bucket object with the lambda function. Source https://github.com/DataDog/datadog-serverless-functions/tree/master/aws/rds_enhanced_monitoring"
  type        = string
  default     = null
}
