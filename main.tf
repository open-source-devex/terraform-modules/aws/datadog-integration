resource "datadog_integration_aws" "integration" {
  account_id = local.aws_account_id
  role_name  = "${local.resource_name_prefix}-datadog-integration-role"

  filter_tags = formatlist("%s:%s", keys(var.filter_tags), values(var.filter_tags))

  account_specific_namespace_rules = var.account_specific_namespace_rules
}
