# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/datadog/datadog" {
  version     = "3.27.0"
  constraints = "~> 3.16"
  hashes = [
    "h1:8iA9Br+pt3Wvb+bDLgYHq7zZ2WYTvxjI128RcUTnqfY=",
    "zh:02ff6e57baf0e4eedab856aeb9def914b2d4fe25a6db908976acf395d17e5d56",
    "zh:07469faec2a03c57936df9e4920bea10b21e0a0a2693009f18753f85dee9adc0",
    "zh:0e7bcf99717f13b4d47c60b93f6aaa8681b65f8ab09ec5949591430e46e5acc7",
    "zh:1b5a20ab6ef92ef41d2995d0274ff913df4d362dc45568d89ae9bd7434b3a4be",
    "zh:357c00199b7679e58e8c817cc5143423a3b353ad1a5cd3a5c4aedcf5702f2bf5",
    "zh:6a5092ef83bb555188db6659c9f7692e0b7d9cc1009b19d28109af32348f44fa",
    "zh:6c195accd2ef793d220e1d3248293f149c3455bbca6f2666bd41fea410cfae80",
    "zh:7d4bb9560f7922ac74c667a6b7565d1d5add373c8254e6276a37f555d9d4bca0",
    "zh:8b1147417ed7e1714699b585fb001c0005c70538adbe596757e6e9d0eb992f5e",
    "zh:95a91082f3fc7f090abae2f6a58c50116fd047be28e7a56b7f18127e3d739390",
    "zh:96d8b4f1645aa2b3e367fd843ec6b0e2b9e5f85554785e5dd3263e2a0fc3ec88",
    "zh:ac1c600fc44ddfc23934f2fffc3066381bfae31453a25becad5e9bf16dff42fc",
    "zh:ba8f6ddee8dd9ffa6a1a808d9256eaef85ef19cf0be81bc238498659d1d78c07",
    "zh:e8ed8969bf78214c1b8c7d9a3aa4d396af66aa2d21d4bb97715fa7c0ee623e71",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "4.67.0"
  constraints = "~> 4.33"
  hashes = [
    "h1:P43vwcDPG99x5WBbmqwUPgfJrfXf6/ucAIbGlRb7k1w=",
    "zh:0843017ecc24385f2b45f2c5fce79dc25b258e50d516877b3affee3bef34f060",
    "zh:19876066cfa60de91834ec569a6448dab8c2518b8a71b5ca870b2444febddac6",
    "zh:24995686b2ad88c1ffaa242e36eee791fc6070e6144f418048c4ce24d0ba5183",
    "zh:4a002990b9f4d6d225d82cb2fb8805789ffef791999ee5d9cb1fef579aeff8f1",
    "zh:559a2b5ace06b878c6de3ecf19b94fbae3512562f7a51e930674b16c2f606e29",
    "zh:6a07da13b86b9753b95d4d8218f6dae874cf34699bca1470d6effbb4dee7f4b7",
    "zh:768b3bfd126c3b77dc975c7c0e5db3207e4f9997cf41aa3385c63206242ba043",
    "zh:7be5177e698d4b547083cc738b977742d70ed68487ce6f49ecd0c94dbf9d1362",
    "zh:8b562a818915fb0d85959257095251a05c76f3467caa3ba95c583ba5fe043f9b",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:9c385d03a958b54e2afd5279cd8c7cbdd2d6ca5c7d6a333e61092331f38af7cf",
    "zh:b3ca45f2821a89af417787df8289cb4314b273d29555ad3b2a5ab98bb4816b3b",
    "zh:da3c317f1db2469615ab40aa6baba63b5643bae7110ff855277a1fb9d8eb4f2c",
    "zh:dc6430622a8dc5cdab359a8704aec81d3825ea1d305bbb3bbd032b1c6adfae0c",
    "zh:fac0d2ddeadf9ec53da87922f666e1e73a603a611c57bcbc4b86ac2821619b1d",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.5.1"
  hashes = [
    "h1:sZ7MTSD4FLekNN2wSNFGpM+5slfvpm5A/NLVZiB7CO0=",
    "zh:04e3fbd610cb52c1017d282531364b9c53ef72b6bc533acb2a90671957324a64",
    "zh:119197103301ebaf7efb91df8f0b6e0dd31e6ff943d231af35ee1831c599188d",
    "zh:4d2b219d09abf3b1bb4df93d399ed156cadd61f44ad3baf5cf2954df2fba0831",
    "zh:6130bdde527587bbe2dcaa7150363e96dbc5250ea20154176d82bc69df5d4ce3",
    "zh:6cc326cd4000f724d3086ee05587e7710f032f94fc9af35e96a386a1c6f2214f",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:b6d88e1d28cf2dfa24e9fdcc3efc77adcdc1c3c3b5c7ce503a423efbdd6de57b",
    "zh:ba74c592622ecbcef9dc2a4d81ed321c4e44cddf7da799faa324da9bf52a22b2",
    "zh:c7c5cde98fe4ef1143bd1b3ec5dc04baf0d4cc3ca2c5c7d40d17c0e9b2076865",
    "zh:dac4bad52c940cd0dfc27893507c1e92393846b024c5a9db159a93c534a3da03",
    "zh:de8febe2a2acd9ac454b844a4106ed295ae9520ef54dc8ed2faf29f12716b602",
    "zh:eab0d0495e7e711cca367f7d4df6e322e6c562fc52151ec931176115b83ed014",
  ]
}
